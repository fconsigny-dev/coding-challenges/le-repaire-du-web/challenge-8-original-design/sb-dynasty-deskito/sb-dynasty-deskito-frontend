import React from "react";
import SBDynastyIcon from "@assets/app-logo/sbn-dynasty.svg";

import './mobile-notfound-navbar.scoped.scss'
export function MobileNotfoundNavbar() {

    return(
        <nav>
            <div id={'navbar-brand'} className={'navbar-brand'}>
                <img src={SBDynastyIcon} alt={'sunburn-dynasty-logo'}/>
                <a href={'/'} className={'deskito-logo-font'}>SunburnDynasty</a>
            </div>

        </nav>
    )
}