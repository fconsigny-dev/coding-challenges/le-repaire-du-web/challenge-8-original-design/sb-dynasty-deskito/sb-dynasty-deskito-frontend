import React from "react";


import NotFoundImage from "@assets/page-not-found/not-found-fishing.svg";

import './mobile-section-not-found.scoped.scss'

export function MobileSectionNotFound() {
    return (
        <section id={'section-not-found'} className={'mobile-section-not-found'}>
            <img src={NotFoundImage} alt={'not-found-image'}/>
            <p className={'deskito-logo-font'}> Oups ! La page demandée n'existe pas </p>
            <a href='/'>Revenir à l'accueil</a>
        </section>

    )
}

