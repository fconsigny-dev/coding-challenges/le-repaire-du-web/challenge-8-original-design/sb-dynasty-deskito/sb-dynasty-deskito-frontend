import React from "react";

import {MobileNotfoundNavbar} from "@application/pages/mobile/not-found/components/navbar/mobile-notfound-navbar";
import {
    MobileSectionNotFound
} from "@application/pages/mobile/not-found/sections/content-not-found/mobile-section-not-found";

import './mobile-page-not-found.scoped.scss'

function MobileNotFoundPage() {

    return (
        <div className={'mobile-page-not-found'}>
            <header id={'navbar'}>
                <MobileNotfoundNavbar/>
            </header>

            <main id={'main-content'} className={'main-container'}>
                <MobileSectionNotFound/>
            </main>

        </div>
    )
}

export default {
    name: 'not-found',
    path: '/*',
    component: MobileNotFoundPage
}