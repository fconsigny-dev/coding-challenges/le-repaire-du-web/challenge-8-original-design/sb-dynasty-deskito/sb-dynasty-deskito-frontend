import React from "react";


import AppleIcon from '@assets/os/application-market-io.svg'
import AndroidIcon from '@assets/os/application-market-android.svg'

import './mobile-landing-home-view.scoped.scss'


//"
function MobileLandingHomeView() {


    return (
        <main id={'mobile-main-view'} className={'mobile-landing-home-view'}>
            <section id={'mobile-landing-home-view-section-title'} className={'mobile-landing-home-view-section-title'}>
                <div className={'title-container'}>
                    <h1 className={'deskito-logo-font'}> Sunburn Dynasty< /h1>
                    <h3 className={'deskito-logo-font'}>Intranet</h3>
                </div>
            </section>

            <section id={'mobile-landing-home-view-download-app-area'}
                     className={'mobile-landing-home-view-download-app-area'}>
                <div className={'download-area-container'}>
                    <h2>Download the mobile App</h2>
                    <button><img src={AppleIcon} alt={'download-apple-store-icon'}/> iOS</button>
                    <button><img src={AndroidIcon} alt={'download-android-store-icon'}/> Android</button>
                </div>
            </section>

            <section id={'mobile-landing-website-call-to-action'} className={'website-call-to-action'}>
                <p>Not an employee ? visit our travel website </p>
                <button>Go to website</button>
            </section>
        </main>
    )
}

export default {
    id: 'MobileLandingHomeView',
    name: 'MobileLandingHomeView',
    view: <MobileLandingHomeView/>
}