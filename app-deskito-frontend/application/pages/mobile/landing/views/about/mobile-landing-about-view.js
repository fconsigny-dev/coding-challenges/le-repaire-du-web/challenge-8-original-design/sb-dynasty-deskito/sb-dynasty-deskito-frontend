import React from "react";

function MobileLandingAboutView() {

    return (
        <main>
            Hello from Contact View
        </main>
    )

}

export default  {
    id: 'MobileLandingAboutView',
    name: 'MobileLandingAboutView',
    view: <MobileLandingAboutView/>
}