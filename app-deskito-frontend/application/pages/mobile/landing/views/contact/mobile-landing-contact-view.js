import React from "react";


import './mobile-landing-contact-view.scoped.scss'

function MobileLandingContactView() {

    return (
        <main>
            Hello from Contact View
        </main>
    )
}

export default {
    id: 'MobileLandingContactView',
    name: 'MobileLandingContactView',
    view: <MobileLandingContactView/>
}