import React, {useState} from 'react'


import MobileLandingHomeView from "@application/pages/mobile/landing/views/home/mobile-landing-home-view";
import {MobileLandingNavbar} from "@application/pages/mobile/landing/components/navbar/mobile-landing-navbar";
import {
    MobileLandingAsideMenu
} from "@application/pages/mobile/landing/components/aside-menu/mobile-landing-aside-menu";
import MobileLandingContactView from "@application/pages/mobile/landing/views/contact/mobile-landing-contact-view";

import './mobile-page-landing.scoped.scss'
import MobileLandingAboutView from "@application/pages/mobile/landing/views/about/mobile-landing-about-view";

function MobileLandingPage() {

    const [currentView, setCurrentView] = useState(MobileLandingHomeView.view);
    const [isBreadCrumbOpen, setIsBreadCrumbOpen] = useState(false)

    function onMenuLinkClick(viewName) {
        setCurrentView(viewName)
        setIsBreadCrumbOpen(false)
    }

    return (
        <div className={'mobile-page-landing'}>
            <header>
                <MobileLandingNavbar onBreadCrumbClick={() => setIsBreadCrumbOpen(!isBreadCrumbOpen)}/>
            </header>
            {
                isBreadCrumbOpen ?
                    <>
                        <div id={'mask'} className={'view-mask'} onClick={() => setIsBreadCrumbOpen(false)}/>
                        <MobileLandingAsideMenu onItemClick={(v) => onMenuLinkClick(v)}/>
                    </>
                    : null

            }
            <main>

                {currentView}
            </main>
        </div>
    )

}

export default {
    name: 'landing',
    path: '/',
    component: MobileLandingPage
}
