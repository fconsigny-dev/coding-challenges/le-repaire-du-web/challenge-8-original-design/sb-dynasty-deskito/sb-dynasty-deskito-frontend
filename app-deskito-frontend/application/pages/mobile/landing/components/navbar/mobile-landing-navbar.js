import React from "react";
import SBDynastyIcon from "@assets/app-logo/sbn-dynasty.svg";
import BreadCrumbIcon from "@assets/navigation/menu-breadcrumb.svg"

import './mobile-landing-navbar.scoped.scss'
export function MobileLandingNavbar({onBreadCrumbClick, onCancelClick}) {

    return (
        <nav id={''} className={'mobile-landing-navbar'}>
            <div id={'navbar-brand'} className={'navbar-brand'}>
                <img src={SBDynastyIcon} alt={'sunburn-dynasty-logo'}/>
                <a href={'/'} className={'deskito-logo-font'}>SunburnDynasty</a>
            </div>

            <div id={'navbar-breadcrumb'} className={'navbar-breadcrumb'} onClick={onBreadCrumbClick}>
                <img src={BreadCrumbIcon} alt={'menu-icon'}/>
            </div>
        </nav>
    )
}