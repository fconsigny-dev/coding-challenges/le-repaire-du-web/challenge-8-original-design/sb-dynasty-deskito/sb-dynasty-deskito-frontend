import React from "react";

import MobileLandingContactView from "@application/pages/mobile/landing/views/contact/mobile-landing-contact-view";
import MobileLandingHomeView from "@application/pages/mobile/landing/views/home/mobile-landing-home-view";

import './mobile-landing-aside-menu.scss'
import MobileLandingAboutView from "@application/pages/mobile/landing/views/about/mobile-landing-about-view";

export function MobileLandingAsideMenu({onItemClick}) {

    return (
        <aside id={'mobile-landing-aside-menu'} className={'mobile-landing-aside-menu'}>
            <ul>
                <li onClick={() => onItemClick(MobileLandingHomeView.view)}> home</li>
                <li onClick={() => onItemClick(MobileLandingContactView.view)}> Contact</li>
                <li onClick={() => onItemClick(MobileLandingAboutView.view)}> About</li>
            </ul>
        </aside>

    )

}