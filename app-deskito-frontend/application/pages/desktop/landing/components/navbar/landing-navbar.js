import React from 'react'
import SBDynastyIcon from "@assets/app-logo/sbn-dynasty.svg";

export function LandingNavbar() {

    return (
        <nav id={''} className={'landing-navbar'}>
            <div id={'navbar-brand'} className={'navbar-brand'}>
                <img src={SBDynastyIcon} alt={'sunburn-dynasty-logo'}/>
                <a href={'/'} className={'deskito-logo-font'}>SunburnDynasty</a>
            </div>
        </nav>
    )
}

//onClick={onBreadCrumbClick}