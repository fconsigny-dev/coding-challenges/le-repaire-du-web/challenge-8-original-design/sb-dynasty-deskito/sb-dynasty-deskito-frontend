import React from "react";

import SBDynastyIcon from '@assets/app-logo/sbn-dynasty.svg'
import {AuthenticationLoginService} from "@business/services/authentication/authentication-login-service";

function LandingLoginView({}) {


    function onAuthenticationValidation(authForm) {
        authForm.preventDefault();
        const login = authForm.target.elements.namedItem('form-login-email').value
        const password = authForm.target.elements.namedItem('form-login-password').value
        const user = AuthenticationLoginService(login,password)
        console.log(user)
    }


    return (
        <>
            <form onSubmit={(form) => onAuthenticationValidation(form)}>
                <img src={SBDynastyIcon} alt={'logo'}/>
                <hr/>
                <input id='form-login-email' type={'email'} required={true}/>
                <input id='form-login-password' type={'password'} required={true}/>
                <button> login</button>
            </form>
        </>
    )
}

export default {
    id: '',
    name: '',
    view: <LandingLoginView/>
}
