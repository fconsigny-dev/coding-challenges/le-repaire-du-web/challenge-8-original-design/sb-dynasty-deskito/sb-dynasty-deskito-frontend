import React, {useState} from 'react'


import {LandingNavbar} from "@application/pages/desktop/landing/components/navbar/landing-navbar";
import LandingLoginView from "@application/pages/desktop/landing/views/login/landing-login-view";

import './page-landing.scoped.scss'

function LandingPage() {

    const [currentView, setCurrentView] = useState(LandingLoginView.view)



    return (
        <div className={'page-landing'}>
            <header>
                <LandingNavbar/>
            </header>

            <main className={'main-container'}>
                {currentView}
            </main>
        </div>
    )

}

export default {
    name: 'landing',
    path: '/',
    component: LandingPage
}
