import React from "react";

import './page-deskito.scoped.scss'

function DeskitoPage () {


    return (
        <>
            Hello From deskito
        </>
    )
}

export default {
    name: 'deskito',
    path: '/deskito',
    component: DeskitoPage
}