import React from "react";

import SBDynastyIcon from "@assets/app-logo/sbn-dynasty.svg";

import './not-found-navbar.scoped.scss'

export function NotFoundNavbar() {

    return (
        <nav>
            <div id='navbar-brand' className={'navbar-brand'}>
                <img src={SBDynastyIcon} alt={'sunburn-dynasty-logo'}/>
                <a href={'/'} className={'deskito-logo-font'}>Sunburn Dynasty</a>
            </div>
        </nav>
    )
}