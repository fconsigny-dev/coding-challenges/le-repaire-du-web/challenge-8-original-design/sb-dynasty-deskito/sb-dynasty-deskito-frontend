import React from 'react'


import {NotFoundNavbar} from "@application/pages/desktop/not-found/components/navbar/not-found-navbar";
import {SectionNotFound} from "@application/pages/desktop/not-found/sections/section-not-found";

import './page-not-found.scoped.scss'

function NotFoundPage() {


    return (
        <div className={'page-not-found'}>
            <header id={'navbar'}>
                <NotFoundNavbar/>
            </header>

            <main id={'main-content'} className={'main-container'}>
                <SectionNotFound/>
            </main>
        </div>
    )
}

export default {
    name: 'not-found',
    path: '/*',
    component: NotFoundPage
}
