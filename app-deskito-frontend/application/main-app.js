import React, {useEffect} from 'react';
import {BrowserRouter as Router, Route, Routes} from "react-router-dom";

// Routing
import pages from './main-manifest'
import mobilePages from './main-mobile-manifest'
// Lang
import TranslatableText from "./main-lang";

// Styles
import './main-theme.scss'

function MainApp() {
    // On charge par défaut le theme de l'application issue des préférences navigateur
    // const theme = window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches ? 'dark' : 'light'

    // On charge au démarrage de l'application la langue par défaut
    const Lang = new TranslatableText().instance();

    function isMobileOrTab() {
        const screenWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
        const screenHeight = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;

        // On suppose que si la largeur de l'écran est inférieure à 600 pixels,
        // l'appareil est un téléphone (à adapter selon vos besoins)
        if (screenHeight < 600) {
            return true;
        }

        // On suppose que si la hauteur de l'écran est supérieure à la largeur,
        // cela indique également un téléphone (à adapter selon vos besoins)
        if (screenHeight > screenWidth) {
            return true;
        }

        return false;
    }

    useEffect(() => {
        //TODO : Nothing here for now on
    }, []);

    return (
        <Router>
            <Routes> {
                isMobileOrTab() ? // L'écran ou l'appareil est de type tablette ou smartphone
                    mobilePages.map(page => (
                        <Route path={page.path} element={<page.component/>} key={page.name}/>
                    ))
                    : // L'écran ou l'appareil est un ordinateur
                    pages.map(page => (
                        <Route path={page.path} element={<page.component/>} key={page.name}/>
                    ))
            }
            </Routes>
        </Router>
    )
}

export default MainApp;
