import i18n from "i18next";
import {initReactI18next} from "react-i18next";

// Detect the lang used by the user browser
import LanguageDetector from 'i18next-browser-languagedetector';

// Language country files
import lang_FR from './lang/fr/fr_fr.json';
import lang_UK from './lang/en/en_uk.json'

class TranslatableText {
    textProvider = null;
    constructor() {

        const resources = {
            "English (UK)": {
                translation: lang_UK
            },
            "Francais (FR)": {
                translation: lang_FR
            }
        };

        this.textProvider = i18n
            .use(LanguageDetector)
            .use(initReactI18next) // passes i18n down to react-i18next
            .init({
                fallbackLng: 'Francais (FR)',
                lng: 'Francais (FR)', // default language,
                resources,
                keySeparator: ".", // to support nested translations,
                interpolation: {
                    escapeValue: false // react already safes from xss
                }
            });
    }

    instance() {
        return this.textProvider;
    }
}

export default TranslatableText;
