import React from 'react';
import ReactDOM from 'react-dom/client';
import MainApp from "./main-app";

const root = ReactDOM.createRoot(document.getElementById('app-deskito'));

root.render(
    <React.StrictMode>
        <MainApp />
    </React.StrictMode>
);


