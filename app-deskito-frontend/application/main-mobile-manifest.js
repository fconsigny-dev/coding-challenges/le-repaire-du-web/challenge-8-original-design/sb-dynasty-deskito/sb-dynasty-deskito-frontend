import MobilePageLanding from "@application/pages/mobile/landing/mobile-page-landing";
import MobilePageNotFound from "@application/pages/mobile/not-found/mobile-page-not-found";

export default [
    MobilePageLanding,
    MobilePageNotFound
]