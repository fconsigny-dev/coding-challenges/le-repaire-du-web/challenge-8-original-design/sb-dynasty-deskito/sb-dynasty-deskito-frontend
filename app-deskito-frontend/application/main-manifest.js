import PageDeskito from "@application/pages/desktop/deskito/page-deskito";
import PageLanding from "@application/pages/desktop/landing/page-landing";
import PageNotFound from "@application/pages/desktop/not-found/page-not-found";

export default [
    PageDeskito,
    PageLanding,
    PageNotFound
]