export enum Language {
    EN_UK = "English (UK)",
    FR = "Francais (FR)",
}

export const LanguageData = {
    [Language.EN_UK]: { key: "UK", value: "English (UK)" },
    [Language.FR]: { key: "FR", value: "Francais (FR)" },
};
