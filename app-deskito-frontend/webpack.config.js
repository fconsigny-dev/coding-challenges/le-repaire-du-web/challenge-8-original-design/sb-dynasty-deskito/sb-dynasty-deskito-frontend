const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

const isProduction = process.env.NODE_ENV === "production";

const config = {
    entry: "./application/main.js",
    output: {
        path: path.resolve(__dirname, "dist"),
        filename: 'bundle.js',
        publicPath: '/'
    },
    devServer: {
        open: false,
        host: "localhost",
        port: 3000,
        historyApiFallback: true
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: "./application/index.html",
            favicon: "./application/main-logo-short.ico"
        }),

        new MiniCssExtractPlugin(),
    ],
    resolve: {
        /** Alias in application **/
        alias: {
            /** App **/
            '@': path.resolve(__dirname, './'),

            /** Pages **/
            '@authentication-page': path.resolve(__dirname, './application/pages/authentication'),
            '@not-found-page': path.resolve(__dirname, './application/pages/not-found'),

            /** Assets & icons **/
            '@assets': path.resolve(__dirname, './application/commons/assets'),
            '@icon': path.resolve(__dirname, './application/commons/icons'),

            /** Styles & Global Component **/
            //!important you should not define global component at least it is immutable
            '@font': path.resolve(__dirname, './application/commons/fonts'),

            /** Languages **/
            '@lang': path.resolve(__dirname, './application/lang'),

            /** Modules **/
            //PRESENTER LAYERS
            '@application': path.resolve(__dirname, './application'),

            //SERVICES LAYERS
            '@business': path.resolve(__dirname, './business'),

            //DATA LAYERS
            '@data': path.resolve(__dirname, './data')

        },
        extensions: [".wasm", ".ts", ".tsx", ".mjs", ".cjs", ".js", ".json", ".jsx"]
    },

    /** Compilation loaders **/
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/i,
                use: {loader: 'babel-loader'},
                exclude: /node_modules/,
            },


            {
                test: /\.(scss)$/,
                use: [
                    // Creates `style` nodes from JS strings
                    "style-loader",
                    // Translates CSS into CommonJS
                    "css-loader",
                    {
                        loader: "postcss-loader",
                        options: {
                            postcssOptions: {
                                plugins: [
                                    [
                                        "postcss-preset-env",
                                        {
                                            browsers: 'last 2 versions'
                                        },
                                    ],
                                ],
                            },
                        },
                    },
                    // Compiles Sass to CSS
                    "sass-loader",
                ],
                exclude: /node_modules/,
            },
            {
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: /node_modules/,
            },
            {
                test: /\.css$/i,
                use: [MiniCssExtractPlugin.loader, "css-loader", "postcss-loader"],
            },
            {
                test: /\.(eot|ttf|woff|woff2)$/i,
                use: ['file-loader'],
                type: "asset",
                exclude: /node_modules/,
            },
            {
                test: /\.(woff|woff2|eot|ttf|otf)$/i,
                type: 'asset/resource',
            },
            {
                test: /\.(svg|png|jpg|gif)$/i,
                use: ['file-loader'],
                exclude: /node_modules/,
            }

        ],
    },
};

module.exports = () => {
    if (isProduction) {
        config.mode = "production";
    } else {
        config.mode = "development";
    }
    return config;
};
